package com.jdtmmn.ondot;

import android.util.Log;

import java.util.Timer;
import java.util.TimerTask;

public class CountDownSeconds extends Timer {

   private static final String TAG = "ONDOT." + CountDownSeconds.class.getSimpleName();
   private int currentVal;
   private int totalTime = 10;
   private IOnSecondPassed callback;

   public interface IOnSecondPassed {
      // pass in the number of seconds that have passed
      void onSecondPassed(int timePassed);
   }

   private CountDownSeconds() {
      super();
   }

   public CountDownSeconds (final int count, final int timePassed, final IOnSecondPassed callback) {

      super();
      totalTime = count;
      this.currentVal = count - timePassed;
      Log.d(TAG, "count = " + count + ", currentVal = " + currentVal);
      this.callback = callback;
      scheduleAtFixedRate(new TimerTask() {
         @Override
         public void run() {
            currentVal--;

            if (callback != null) {
               callback.onSecondPassed(totalTime - currentVal);
            }
            if (currentVal == 1) {
               cancel();
            }
         }
      }, 1000, 1000);
   }
}
