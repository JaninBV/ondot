package com.jdtmmn.ondot;

import android.os.PersistableBundle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements CountDownSeconds.IOnSecondPassed {

   private static final String TAG = "ONDOT." + MainActivity.class.getSimpleName();
   TextView counterTV;
   private int lastTimePassed = 0;
   private static final String TIME_PASSED_KEY = "TimePassed";
   private CountDownSeconds countDown;

   @Override
   protected void onCreate(Bundle savedInstanceState) {
      super.onCreate(savedInstanceState);
      setContentView(R.layout.activity_main);
      counterTV = findViewById(R.id.counter);
      if (savedInstanceState != null) {
         lastTimePassed = savedInstanceState.getInt(TIME_PASSED_KEY, 0);
         Log.d(TAG, "onCreate():  savedInstanceState wasn't null, lastTimePassed = " + lastTimePassed);
      }

      Log.d(TAG, "onCreate():  lastTimePassed = " + lastTimePassed);
      if (lastTimePassed < 9) {
         counterTV.setText(Integer.toString(10 - lastTimePassed));
         countDown = new CountDownSeconds(10, lastTimePassed,this);
      }
   }

   @Override
   public void onSecondPassed(final int timePassed) {

      Log.d(TAG, "onSecondPassed():  timePassed = " + timePassed);
      lastTimePassed = timePassed;
      final int time = 10 - timePassed;

      runOnUiThread(new Runnable() {
         @Override
         public void run() {
            counterTV.setText(Integer.toString(time));
         }
      });
   }

   @Override
   public void onSaveInstanceState(Bundle outState) {

      outState.putInt(TIME_PASSED_KEY, lastTimePassed);
      if (countDown != null) {
         countDown.cancel();
         countDown = null;
      }
      super.onSaveInstanceState(outState);
   }
}
